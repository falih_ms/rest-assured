import io.restassured.RestAssured;
import io.restassured.RestAssured.*;
import io.restassured.http.ContentType;
import io.restassured.matcher.RestAssuredMatchers.*;
import io.restassured.response.Response;

import static org.hamcrest.Matchers.*;

import org.hamcrest.core.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class Validation {
    public static Response response;
    public static String jsonAsString;	
	
    public PropertiesReader rp;
	private String name;
	private String validMoves;
	private String invalidMoves;

	@BeforeClass
    public void beforeClass() {
    	rp = new PropertiesReader("./resource/config.properties");
    	name = rp.getData("name"); 
    	validMoves = rp.getData("validMoves"); 
    	invalidMoves = rp.getData("invalidMoves"); 
    	
    }
    
    @Test
    public void ValidMoves()
    {
        get("https://pokeapi.co/api/v2/pokemon/"+ name +"/")
        .then()
        .assertThat()
        .body("moves.move.name", hasItems(validMoves));
    }

    @Test
    public void InvalidMoves()
    {
        get("https://pokeapi.co/api/v2/pokemon/"+ name +"/")
        .then()
        .assertThat()
        .body("moves.move.name", not(hasItems(invalidMoves)));
    }


}
