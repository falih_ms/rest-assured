

import java.io.FileInputStream;
import java.util.Properties;

public class PropertiesReader {
	private Properties prop;
	public PropertiesReader(String file) {
		try {
			FileInputStream is = new FileInputStream(file);
			prop = new Properties();
			prop.load(is);
					
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getData(String data) {
		return prop.getProperty(data);
	}
}
